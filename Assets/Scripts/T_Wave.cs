﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class T_Wave : MonoBehaviour
{
    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    // Use this for initialization
    void Start()
    {
        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);
    }

    // Update is called once per frame
    void Update()
    {
        if( m_joyconR.GetButton(Joycon.Button.DPAD_RIGHT) )
        {
            gameObject.GetComponent<SphereCollider>().enabled = true;
            m_joyconR.SetRumble(160, 320, 0.6f, 1);

        }
        else
        {
            gameObject.GetComponent<SphereCollider>().enabled = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        // 壊れるオブジェクトに当たったら
        if (other.gameObject.tag == "BrokenObj")
        {
            // とりあえず消す
            Destroy(other.gameObject);
        }
    }
}

