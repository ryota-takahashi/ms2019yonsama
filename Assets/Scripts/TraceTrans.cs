﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceTrans : MonoBehaviour {

    [SerializeField]
    GameObject Target;

    [SerializeField]
    Vector3 Offset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        

        transform.localPosition = Target.transform.localPosition + Offset;
    }
}
