﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class AutoMove : MonoBehaviour {

    UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter m_Player;

    [SerializeField]
    Vector3 Speed;

    [SerializeField, Range(-5, 0)]
    float m_LeftLimit;

    [SerializeField, Range(0, 5)]
    float m_RightLimit;

    [SerializeField, Range(0.0f, 1.0f)]
    float m_JoyconRotLimit;

    private List<Joycon> m_joycons;
    private Joycon m_joyconL;
    private Joycon m_joyconR;

    // Use this for initialization
    void Start () {
        m_Player = gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();

        m_joycons = JoyconManager.Instance.j;

        if (m_joycons == null || m_joycons.Count <= 0) return;

        m_joyconL = m_joycons.Find(c => c.isLeft);
        m_joyconR = m_joycons.Find(c => !c.isLeft);

    }
	
	// Update is called once per frame
	void Update () {

        Vector3 moveVar = Vector3.zero;

        // ジョイコン(右)のZ軸の傾きを取得
        float fAccelZ = m_joyconR.GetAccel().z;

        fAccelZ = Mathf.Max(fAccelZ, -m_JoyconRotLimit);
        fAccelZ = Mathf.Min(fAccelZ, m_JoyconRotLimit);



        moveVar = gameObject.transform.localPosition;
        moveVar.x = -fAccelZ * 2;

        gameObject.transform.localPosition = moveVar;
    }

    private void FixedUpdate()
    {
        m_Player.Move(Speed, false, false);
    }
}
